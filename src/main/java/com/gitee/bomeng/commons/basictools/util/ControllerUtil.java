package com.gitee.bomeng.commons.basictools.util;

import com.alibaba.fastjson.JSON;
import com.gitee.bomeng.commons.basictools.AbstractHttpResContent;
import com.gitee.bomeng.commons.basictools.Constant;
import com.gitee.bomeng.commons.basictools.bean.CommonException;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

import static org.apache.http.HttpStatus.SC_NOT_IMPLEMENTED;

/**
 * User:gfb
 * Date:2017/11/16
 * Desc:
 *
 * @author gfb
 */
public class ControllerUtil {
    /**
     * 日志
     */
    private static final Logger logger = LoggerFactory.getLogger(ControllerUtil.class);
    /**
     * STRING_NOT_BLANK
     */
    public static final String R_P_STRING_NOT_BLANK = "STRING_NOT_BLANK";

    private ControllerUtil() {
    }

    /**
     * save错误信息和数据
     *
     * @param abstractHttpResContent abstractHttpResContent
     * @param e                      异常
     * @param <T>                    泛型
     */
    public static <T> void saveErrorMsgAndData(AbstractHttpResContent<T> abstractHttpResContent, Exception e) {
        String errorMsg;
        if (CommonException.class.isInstance(e)) {
            CommonException commonException = CommonException.class.cast(e);
            errorMsg = String.format("%s:Fail;ErrorCode:%s;ErrorMsg:%s", abstractHttpResContent.getFuncDes(), commonException.getErrorCode(), commonException.getErrorMessage());
            abstractHttpResContent.fillData(commonException.getErrorCode(), commonException.getErrorMessage(), null);
        } else {
            errorMsg = String.format("%s:Fail;ErrorMsg:%s", abstractHttpResContent.getFuncDes(), e.getMessage());
            abstractHttpResContent.fillData(SC_NOT_IMPLEMENTED, errorMsg, null);
        }
        // 控制台输出的打印信息
        logger.error(errorMsg);
    }

    /**
     * save成功信息和数据
     *
     * @param abstractHttpResContent 抽象abstractHttpResContent
     * @param data                   类型
     * @param <T>                    泛型
     */
    public static <T> void saveSuccessMsgAndData(AbstractHttpResContent<T> abstractHttpResContent, T data) {
        String info = String.format("%s:Success!", abstractHttpResContent.getFuncDes());
        logger.info(info);
        abstractHttpResContent.fillData(HttpStatus.SC_OK, info, data);
    }

    /**
     * 检查参数合法性
     *
     * @param requestParamType 参数类型
     * @param pName            参数名
     * @param pValue           参数值
     * @throws IllegalArgumentException 非法传参异常
     */
    public static void checkParamValid(String requestParamType, String pName, String pValue) throws IllegalArgumentException {
        if (R_P_STRING_NOT_BLANK.equals(requestParamType) && StringUtils.isBlank(pValue)) {
            throw new IllegalArgumentException(String.format("参数不合法[%s:%s]", pName, pValue));
        }
    }

    /**
     * 写出结果返回(默认不允许跨域)
     *
     * @param abstractHttpResContent abstractHttpResContent
     * @param <T>                    泛型
     */
    public static <T> void writeResult(AbstractHttpResContent<T> abstractHttpResContent) {
        writeResult(abstractHttpResContent, null);
    }


    /**
     * 写出结果返回
     *
     * @param abstractHttpResContent abstractHttpResContent
     * @param allowOrigin            允许跨域
     * @param <T>                    泛型
     */
    public static <T> void writeResult(AbstractHttpResContent<T> abstractHttpResContent, String allowOrigin) {
        try {
            HttpServletResponse response = abstractHttpResContent.getResponse();
            if (StringUtils.isNotBlank(allowOrigin)) {
                response.setHeader(Constant.ALLOW_ORIGIN, allowOrigin);
            }
            response.setContentType(Constant.CONTENT_TYPE_APPLICATION_JSON);
            response.setCharacterEncoding(Constant.DEFAULT_ENCODE_UTF8);
            PrintWriter printWriter = response.getWriter();
            printWriter.write(JSON.toJSONString(abstractHttpResContent));
            printWriter.close();
            printWriter.flush();
        } catch (Exception e) {
            logger.error("writeResult返回数据异常:{}", e.getMessage());
        }
    }
}
