package com.gitee.bomeng.commons.basictools.bean;

/**
 * User:gfb
 * Date:2018/5/22
 * Desc:
 */
public class CommonException extends Exception  {
    private static final long serialVersionUID = -6669767141032947671L;

    private Integer errorCode;
    private String errorMessage;

    public CommonException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public CommonException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
