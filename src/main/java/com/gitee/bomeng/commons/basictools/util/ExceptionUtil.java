package com.gitee.bomeng.commons.basictools.util;

import com.gitee.bomeng.commons.basictools.bean.CommonException;
import com.gitee.bomeng.commons.basictools.bean.CommonRuntimeException;

/**
 * User:gfb
 * Date:2018/5/22
 * Desc:
 *
 * @author gfb
 */
public class ExceptionUtil {

    private ExceptionUtil() {
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorCode    异常码
     * @param errorMessage 异常信息
     * @throws CommonRuntimeException 通用异常
     */
    public static void throwNewCommonRuntimeException(int errorCode, String errorMessage) throws CommonRuntimeException {
        throw new CommonRuntimeException(errorCode, errorMessage);
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorCode          异常码
     * @param errorMessageFormat 异常信息
     * @param args               参数填充(均不能为null)
     * @throws CommonRuntimeException 通用异常
     */
    public static void throwNewCommonRuntimeException(int errorCode, String errorMessageFormat, Object... args) throws CommonRuntimeException {
        throw new CommonRuntimeException(errorCode, String.format(errorMessageFormat, args));
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorMessage 异常信息
     * @throws CommonRuntimeException 通用异常
     */
    public static void throwNewCommonRuntimeException(String errorMessage) throws CommonRuntimeException {
        throw new CommonRuntimeException(errorMessage);
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorMessageFormat 异常信息format
     * @param args               参数填充(均不能为null)
     * @throws CommonRuntimeException 通用异常
     */
    public static void throwNewCommonRuntimeException(String errorMessageFormat, Object... args) throws CommonRuntimeException {
        throw new CommonRuntimeException(String.format(errorMessageFormat, args));
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorCode    异常码
     * @param errorMessage 异常信息
     * @throws CommonException 通用异常
     */
    public static void throwNewCommonException(int errorCode, String errorMessage) throws CommonException {
        throw new CommonException(errorCode, errorMessage);
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorCode          异常码
     * @param errorMessageFormat 异常信息
     * @param args               参数填充(均不能为null)
     * @throws CommonException 通用异常
     */
    public static void throwNewCommonException(int errorCode, String errorMessageFormat, Object... args) throws CommonException {
        throw new CommonException(errorCode, String.format(errorMessageFormat, args));
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorMessage 异常信息
     * @throws CommonException 通用异常
     */
    public static void throwNewCommonException(String errorMessage) throws CommonException {
        throw new CommonException(errorMessage);
    }

    /**
     * 抛出一个新的通用异常
     *
     * @param errorMessageFormat 异常信息format
     * @param args               参数填充(均不能为null)
     * @throws CommonException 通用异常
     */
    public static void throwNewCommonException(String errorMessageFormat, Object... args) throws CommonException {
        throw new CommonException(String.format(errorMessageFormat, args));
    }

}
