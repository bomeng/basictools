package com.gitee.bomeng.commons.basictools;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * User:gfb
 * Date:2018/5/22
 * Desc:
 *
 * @author gfb
 */
public class AbstractHttpResContent<T> implements Serializable {
    private static final long serialVersionUID = -5519468670642188376L;
    /**
     * 返回值code
     */
    private Integer code;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 返回的结果数据
     */
    private T result;
    /*
     * ***************上边三个值为必须返回的数据,下边的均不能返回****************
     */
    /**
     * 调用接口方法描述
     */
    private transient String funcDes;
    /**
     * 用户IP
     */
    private transient String userIp;
    /**
     * 请求URL,如http:localhost:8080/hello_word.do
     */
    private transient String reqURL;
    /**
     * 请求URI,如/hello_word.do
     */
    private transient String reqURI;
    /**
     * 包含请求参数的URL,如http:localhost:8080/hello_word.do?user_name=XXX
     */
    private transient String reqQueryURL;
    /**
     * 包含请求参数的URI,如/hello_word.do?user_name=XXX
     */
    private transient String reqQueryURI;
    /**
     * 请求的参数,如?user_name=XXX
     */
    private transient String reqQuery;
    /**
     * 请求方式,如POST
     */
    private transient String reqMethod;
    /**
     * 请求参数JSON字符串格式,如{"user_name":["XXX"]}
     */
    private transient String reqParams;
    /**
     * 请求SessionId
     */
    private transient String sessionId;
    /**
     * 请求Session
     */
    private transient HttpSession httpSession;
    /**
     * HttpServletRequest
     */
    private transient HttpServletRequest request;
    /**
     * HttpServletResponse
     */
    private transient HttpServletResponse response;
    /**
     * 用户对象
     */
    protected transient Object userInfo;
    /**
     * 是否登录
     */
    protected transient Boolean isLogin;

    AbstractHttpResContent(HttpServletRequest request, HttpServletResponse response) {
        this.code = 200;
        this.msg = "";
        this.reqURI = "";
        this.reqURL = "";
        this.reqQueryURI = "";
        this.reqQueryURL = "";
        this.reqParams = "";
        this.userIp = "";
        this.isLogin = false;
        this.response = response;
        this.request = request;
        if (request != null) {
            this.reqQuery = StringUtils.isNotBlank(request.getQueryString()) ? "?" + request.getQueryString() : "";
            this.reqURI = request.getRequestURI();
            this.reqURL = request.getRequestURL().toString();
            this.reqQueryURI = this.reqURI + this.reqQuery;
            this.reqQueryURL = this.reqURL + this.reqQuery;
            this.reqParams = JSON.toJSONString(request.getParameterMap());
            this.reqMethod = request.getMethod();
            this.httpSession = request.getSession();
            this.userIp = this.getIpAddress(request);
            if (this.httpSession != null) {
                this.sessionId = this.httpSession.getId();
                this.userInfo = this.httpSession.getAttribute(Constant.SESSION_CURRENT_LOGIN_USER_INFO);
            }
            this.isLogin = this.userInfo != null;
        }
    }

    /**
     * 填充用于返回的数据
     *
     * @param code   code
     * @param msg    信息描述
     * @param result 结果
     */
    public void fillData(Integer code, String msg, T result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    /**
     * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址;
     */
    private String getIpAddress(HttpServletRequest request) {
        // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (String strIp : ips) {
                if (!("unknown".equalsIgnoreCase(strIp))) {
                    ip = strIp;
                    break;
                }
            }
        }
        return ip;
    }

    public void setFuncDes(String funcDes) {
        this.funcDes = funcDes;
    }

    public String getFuncDes() {
        return funcDes;
    }

    public String getUserIp() {
        return userIp;
    }


    public String getReqURI() {
        return reqURI;
    }

    public String getReqQueryURL() {
        return reqQueryURL;
    }

    public String getReqQueryURI() {
        return reqQueryURI;
    }

    public String getReqMethod() {
        return reqMethod;
    }


    public String getReqParams() {
        return reqParams;
    }

    public String getSessionId() {
        return sessionId;
    }

    public HttpSession getHttpSession() {
        return httpSession;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    // Getter方法用于JSON串行化
    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getResult() {
        return result;
    }

}
