package com.gitee.bomeng.commons.basictools.bean;

import com.gitee.bomeng.commons.basictools.Enum.ExcelTypeEnum;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * User:gfb
 * Date:2018/5/28
 * Desc: Excel bean,继承LinkedList&lt;ExcelSheetBean&gt;，因为增删比遍历可能更频繁
 *
 * @author gfb
 */
public class ExcelBean extends LinkedList<ExcelSheetBean> implements Serializable {
    private static final long serialVersionUID = -293790869871416005L;
    /**
     * Excel扩展名
     */
    private ExcelTypeEnum excelTypeEnum;

    public ExcelBean() {
        excelTypeEnum = ExcelTypeEnum.XLSX;
    }

    /**
     * 增加工作表
     *
     * @param excelSheetBean 工作表
     * @return ExcelBean
     */
    public ExcelBean addExcelSheetBeanAndReturnExcelBean(ExcelSheetBean excelSheetBean) {
        super.add(excelSheetBean);
        excelSheetBean.setExcelBean(this);
        return this;
    }

    /**
     * 增加指定sheetNum的工作表
     *
     * @param sheetNum       工作表号,从1开始
     * @param excelSheetBean 工作表
     */
    public void addExcelSheetBean(int sheetNum, ExcelSheetBean excelSheetBean) {
        if (sheetNum < 1) {
            return;
        }
        super.add(sheetNum - 1, excelSheetBean);
        excelSheetBean.setExcelBean(this);
    }

    @Override
    public boolean add(ExcelSheetBean excelSheetBean) {
        addExcelSheetBeanAndReturnExcelBean(excelSheetBean);
        return true;
    }

    @Override
    public void add(int index, ExcelSheetBean excelSheetBean) {
        addExcelSheetBean(index, excelSheetBean);
    }

    /**
     * 批量增加工作表
     *
     * @param excelSheetBeanList 工作表集合
     */
    public void addExcelSheetBeanList(List<ExcelSheetBean> excelSheetBeanList) {
        for (ExcelSheetBean excelSheetBean : excelSheetBeanList) {
            addExcelSheetBeanAndReturnExcelBean(excelSheetBean);
        }
    }

    /**
     * 移除出现第一个ExcelSheetBean对象的工作表
     *
     * @param excelSheetBean ExcelSheetBean
     * @return boolean
     */
    public boolean removeExcelSheetBean(ExcelSheetBean excelSheetBean) {
        return super.remove(excelSheetBean);
    }

    /**
     * 根据sheetNum获取工作表
     *
     * @param sheetNum sheetNum
     * @return ExcelSheetBean
     */
    public ExcelSheetBean getExcelSheetBeanBySheetNum(int sheetNum) {
        return super.get(sheetNum - 1);
    }

    /**
     * 获取Excel类型
     *
     * @return ExcelTypeEnum
     */
    public ExcelTypeEnum getExcelTypeEnum() {
        return excelTypeEnum;
    }

    /**
     * 设置Excel类型
     *
     * @param excelTypeEnum ExcelTypeEnum
     */
    public void setExcelTypeEnum(ExcelTypeEnum excelTypeEnum) {
        this.excelTypeEnum = excelTypeEnum;
    }
}
