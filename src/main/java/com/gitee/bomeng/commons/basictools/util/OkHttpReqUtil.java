package com.gitee.bomeng.commons.basictools.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import okhttp3.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User:gfb
 * Date:2018-12-21
 * Desc:
 *
 * @author gfb
 */
public class OkHttpReqUtil {
    private static final Pattern J_SESSION_ID_REGEX = Pattern.compile("JSESSIONID=[\\w]{32}");

    public static final String JSESSIONID = "JSESSIONID";
    public static final String SET_COOKIE = "Set-Cookie";
    public static final String COOKIE = "Cookie";

    /**
     * IP
     */
    private static final int[][] IP_RANGE = {
            // 36.56.0.0-36.63.255.255
            {607649792, 608174079},
            // 61.232.0.0-61.237.255.255
            {1038614528, 1039007743},
            // 106.80.0.0-106.95.255.255
            {1783627776, 1784676351},
            // 121.76.0.0-121.77.255.255
            {2035023872, 2035154943},
            // 123.232.0.0-123.235.255.255
            {2078801920, 2079064063},
            // 139.196.0.0-139.215.255.255
            {-1950089216, -1948778497},
            // 171.8.0.0-171.15.255.255
            {-1425539072, -1425014785},
            // 182.80.0.0-182.92.255.255
            {-1236271104, -1235419137},
            // 210.25.0.0-210.47.255.255
            {-770113536, -768606209},
            // 222.16.0.0-222.95.255.255
            {-569376768, -564133889},
    };
    public static final String HEADER_X_FORWARDED_FOR_KEY = "x-forwarded-for";
    /**
     * User Agent
     */
    public static final String HEADER_USER_AGENT_KEY = "User-Agent";
    private static final String[] HEADER_USER_AGENT_VALUE = {
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Mozilla/5.0 (Linux; Android 8.1; PACT00 Build/O11019; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/044030 Mobile Safari/537.36 MicroMessenger/6.6.6.1300(0x26060637) NetType/WIFI Language/zh_CN MicroMessenger/6.6.6.1300(0x26060637) NetType/WIFI Language/zh_CN",
            "Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_6 like Mac OS X) AppleWebKit/604.5.6 (KHTML, like Gecko) Mobile/15D100 MicroMessenger/6.6.6 NetType/WIFI Language/zh_CN",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:64.0) Gecko/20100101 Firefox/64.0",
            "Mozilla/5.0 (iPhone; CPU iPhone OS 11_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E302"
    };

    public static final MediaType MEDIA_TYPE = MediaType.parse("application/octet-stream");
    /**
     * 请求方式
     */
    public static final byte POST = 1;
    public static final byte GET = 2;

    private OkHttpReqUtil() {
    }

    // ****获取参数Map****

    /**
     * 获取参数JSONObject
     *
     * @param args 参数
     * @return JSONObject
     */
    public static JSONObject getParamsJSONObject(String... args) {
        JSONObject reqParams = new JSONObject((int) (args.length * 0.75 + 1));
        if (args.length <= 1) {
            return null;
        }
        for (int i = 0; i < args.length; i += 2) {
            reqParams.put(args[i], args[i + 1]);
        }
        return reqParams;
    }

    /**
     * 获取参数JSONObject
     *
     * @param paramsString 参数,如chooser=seq&amp;allcoll=n
     * @return JSONObject
     */
    public static JSONObject getParamsJSONObject(String paramsString) {
        JSONObject reqParams = new JSONObject();
        if (StringUtils.isNotBlank(paramsString)) {
            String[] split = StringUtils.split(paramsString, "&");
            List<String> paramsKeyValueList = new ArrayList<>(split.length * 2);
            for (String headerKeyValue : split) {
                String[] keyAndValue = headerKeyValue.split("=");
                paramsKeyValueList.add(StringUtils.trim(keyAndValue[0]));
                paramsKeyValueList.add(keyAndValue.length <= 1 ? "" : keyAndValue[1]);
            }
            String[] headerKeyAndValueArray = new String[paramsKeyValueList.size()];
            headerKeyAndValueArray = paramsKeyValueList.toArray(headerKeyAndValueArray);
            reqParams = getParamsJSONObject(headerKeyAndValueArray);
        }

        return reqParams;
    }

    // ****获取HEADER****

    /**
     * 获取HeaderMap
     *
     * @param args header
     * @return Map
     */
    public static Map<String, String> getHeaderMap(String... args) {
        Map<String, String> headerMap = new HashMap<>();
        if (args.length <= 1) {
            return null;
        }
        for (int i = 0; i < args.length; i += 2) {
            headerMap.put(args[i], args[i + 1]);
        }
        return headerMap;
    }

    /**
     * 获取HeaderMap
     *
     * @param headerString 如：Host: corpus.byu.edu
     *                     Connection: keep-alive
     * @return Map
     */
    public static Map<String, String> getHeaderMap(String headerString) {
        Map<String, String> headerMap = new HashMap<>();
        if (StringUtils.isNotBlank(headerString)) {
            String[] split = StringUtils.split(headerString, System.lineSeparator());
            List<String> headerKeyValueList = new ArrayList<>(split.length * 2);
            for (String headerKeyValue : split) {
                String[] keyAndValue = headerKeyValue.split(": ");
                headerKeyValueList.add(StringUtils.trim(keyAndValue[0]));
                headerKeyValueList.add(keyAndValue.length <= 1 ? "" : keyAndValue[1]);
            }
            String[] headerKeyAndValueArray = new String[headerKeyValueList.size()];
            headerKeyAndValueArray = headerKeyValueList.toArray(headerKeyAndValueArray);
            headerMap = getHeaderMap(headerKeyAndValueArray);
        }
        return headerMap;
    }

    /**
     * 获取随机IP
     *
     * @return IP字符串
     */
    public static String getRandomIP() {
        Random random = new Random();
        int index = random.nextInt(10);
        return numToIP(IP_RANGE[index][0] + new Random().nextInt(IP_RANGE[index][1] - IP_RANGE[index][0]));
    }

    /**
     * 将数字转换为IP格式
     *
     * @param ipNum 数字
     * @return IP格式字符串
     */
    private static String numToIP(int ipNum) {
        int[] b = new int[4];
        String x = "";
        b[0] = (ipNum >> 24) & 0xff;
        b[1] = (ipNum >> 16) & 0xff;
        b[2] = (ipNum >> 8) & 0xff;
        b[3] = ipNum & 0xff;
        x = b[0] + "." + b[1] + "." + b[2] + "." + b[3];
        return x;
    }

    /**
     * 随机返回UserAgent
     *
     * @return UserAgent
     */
    public static String getRandomUserAgentValue() {
        return HEADER_USER_AGENT_VALUE[RandomUtils.nextInt(HEADER_USER_AGENT_VALUE.length)];
    }

    // ****文件上传****

    /**
     * 创建文件上传对象集合
     *
     * @param uploadFileBeans uploadFileBeans
     * @return List&lt;UploadFileBean&gt;
     */
    public static List<UploadFileBean> createUploadFileBeanList(UploadFileBean... uploadFileBeans) {
        return new ArrayList<>(Arrays.asList(uploadFileBeans));
    }

    /**
     * 同步方式上传文件并返回JSONObject
     *
     * @param urlStr             urlStr
     * @param urlParams          urlParams
     * @param reqParams          reqParams
     * @param uploadFileBeanList uploadFileBeanList
     * @return JSONObject
     */
    public static JSONObject uploadFileAndReturnJSONObjectBySync(String urlStr, JSONObject urlParams, JSONObject reqParams, List<UploadFileBean> uploadFileBeanList) {
        Response response = uploadFileAndReturnResponseBySync(urlStr, urlParams, reqParams, uploadFileBeanList, null, null);
        return getJSONObjectFromResponse(response);
    }

    /**
     * 同步方式上传文件并返回Response
     *
     * @param urlStr             urlStr
     * @param urlParams          urlParams
     * @param reqParams          reqParams
     * @param uploadFileBeanList uploadFileBeanList
     * @param headerMap          headerMap
     * @param addHeaderMap       addHeaderMap
     * @return Response
     */
    public static Response uploadFileAndReturnResponseBySync(String urlStr, JSONObject urlParams, JSONObject reqParams, List<UploadFileBean> uploadFileBeanList, Map<String, String> headerMap, Map<String, String> addHeaderMap) {
        String urlParamsString = buildUrlParams(urlParams);
        urlStr += urlParamsString;

        MultipartBody.Builder multipartBodyBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        // 参数
        if (reqParams != null && reqParams.size() > 0) {
            multipartBodyBuilder.addPart(buildFormBodyParams(reqParams));
        }
        // 文件
        if (uploadFileBeanList != null && uploadFileBeanList.size() > 0) {
            for (UploadFileBean uploadFileBean : uploadFileBeanList) {
                multipartBodyBuilder = multipartBodyBuilder.addFormDataPart(uploadFileBean.getParamName(), uploadFileBean.getFileName(), RequestBody.create(MEDIA_TYPE, uploadFileBean.getFileContentBytes()));
            }
        }
        return buildSyncRequestAndReturnResponse(urlStr, POST, multipartBodyBuilder.build(), headerMap, addHeaderMap);
    }

    // ****GET****

    /**
     * 发送同步GET请求并返回String
     *
     * @param urlStr    urlStr
     * @param urlParams urlParams
     * @return String
     */
    public static String sendSyncGetRequestAndReturnString(String urlStr, JSONObject urlParams) {
        return getStringFromResponse(buildSyncRequestAndReturnResponse(urlStr + buildUrlParams(urlParams), GET, null, null, null));
    }

    /**
     * 发送同步GET请求并返回String
     *
     * @param urlStr       urlStr
     * @param urlParams    urlParams
     * @param headerMap    headerMap原有Header
     * @param addHeaderMap addHeaderMap新增的Header
     * @return String
     */
    public static String sendSyncGetRequestAndReturnString(String urlStr, JSONObject urlParams, Map<String, String> headerMap, Map<String, String> addHeaderMap) {
        return getStringFromResponse(buildSyncRequestAndReturnResponse(urlStr + buildUrlParams(urlParams), GET, null, headerMap, addHeaderMap));
    }

    /**
     * 发送同步GET请求并返回Response
     *
     * @param urlStr       urlStr
     * @param urlParams    urlParams
     * @param headerMap    headerMap原有Header
     * @param addHeaderMap addHeaderMap新增的Header
     * @return Response
     */
    public static Response sendSyncGetRequestAndReturnResponse(String urlStr, JSONObject urlParams, Map<String, String> headerMap, Map<String, String> addHeaderMap) {
        return buildSyncRequestAndReturnResponse(urlStr + buildUrlParams(urlParams), GET, null, headerMap, addHeaderMap);
    }

    // ****POST****

    /**
     * 发送同步post请求，并返回JSONObject
     *
     * @param urlStr    urlStr
     * @param reqParams 请求参数
     * @return JSONObject
     */
    public static JSONObject sendSyncPostRequestAndReturnJSONObject(String urlStr, JSONObject reqParams) {
        return sendSyncPostRequestAndReturnJSONObject(urlStr, null, reqParams);
    }

    /**
     * 发送同步post请求，并返回JSONObject
     *
     * @param urlStr    urlStr
     * @param urlParams urlParams
     * @param reqParams 请求参数
     * @return JSONObject
     */
    public static JSONObject sendSyncPostRequestAndReturnJSONObject(String urlStr, JSONObject urlParams, JSONObject reqParams) {
        return sendSyncPostRequestAndReturnJSONObject(urlStr, urlParams, reqParams, null);
    }

    /**
     * 发送同步post请求，并返回JSONObject
     *
     * @param urlStr    urlStr
     * @param urlParams urlParams
     * @param reqParams 请求参数
     * @param headerMap 请求头，可为null
     * @return JSONObject
     */
    public static JSONObject sendSyncPostRequestAndReturnJSONObject(String urlStr, JSONObject urlParams, JSONObject reqParams, Map<String, String> headerMap) {
        return getJSONObjectFromResponse(sendSyncPostRequestAndReturnResponse(urlStr, urlParams, reqParams, headerMap, null));
    }

    /**
     * 发送同步的请求并返回response
     *
     * @param urlStr       urlStr
     * @param urlParams    urlParams
     * @param reqParams    请求参数，可为null
     * @param headerMap    请求头，可为null，原有的header修改
     * @param addHeaderMap 新增请求头,可为null
     * @return response 异常时返回null
     */
    public static Response sendSyncPostRequestAndReturnResponse(String urlStr, JSONObject urlParams, JSONObject reqParams, Map<String, String> headerMap, Map<String, String> addHeaderMap) {
        return buildSyncRequestAndReturnResponse(urlStr + buildUrlParams(urlParams), POST, buildFormBodyParams(reqParams), headerMap, addHeaderMap);
    }

    /**
     * 发送同步的请求并返回response
     *
     * @param urlStr           urlStr
     * @param urlParams        urlParams
     * @param reqParams        请求参数，可为null
     * @param headerMap        请求头，可为null，原有的header修改
     * @param addHeaderMap     新增请求头,可为null
     * @param sslSocketFactory sslSocketFactory
     * @param hostnameVerifier hostnameVerifier
     * @return response 异常时返回null
     */
    public static Response sendSyncPostRequestAndReturnResponse(String urlStr, JSONObject urlParams, JSONObject reqParams, Map<String, String> headerMap, Map<String, String> addHeaderMap, SSLSocketFactory sslSocketFactory, HostnameVerifier hostnameVerifier) {
        return buildSyncRequestAndReturnResponse(urlStr + buildUrlParams(urlParams), POST, buildFormBodyParams(reqParams), headerMap, addHeaderMap, sslSocketFactory, hostnameVerifier);
    }


    /**
     * 从Response获取JSESSIONID
     *
     * @param response response
     * @return String
     */
    public static String getJSESSIONIDFromResponse(Response response) {
        return getJSESSIONIDBySetCookie(response.headers().get(SET_COOKIE));
    }

    /**
     * 根据Response获取JSONObject
     *
     * @param response response
     * @return JSONObject
     */
    public static JSONObject getJSONObjectFromResponse(Response response) {
        if (response == null) {
            return null;
        }
        if (!response.isSuccessful()) {
            System.out.println(String.valueOf(response));
        }
        JSONObject rjo;
        ResponseBody responseBody = response.body();
        if (responseBody == null) {
            rjo = new JSONObject();
        } else {
            try {
                rjo = JSON.parseObject(responseBody.string());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return rjo;
    }

    /**
     * 根据Response获取String
     *
     * @param response response
     * @return JSONObject
     */
    public static String getStringFromResponse(Response response) {
        if (response == null) {
            return null;
        }
        if (!response.isSuccessful()) {
            System.out.println(response);
        }
        ResponseBody responseBody = response.body();
        if (responseBody != null) {
            try {
                return responseBody.string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    /**
     * 创建SSLSocketFactory对象
     *
     * @return SSLSocketFactory
     */
    public static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory sslsocketfactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{new TrustAllManager()},
                    new SecureRandom());
            sslsocketfactory = sc.getSocketFactory();
        } catch (Exception ignored) {
        }
        return sslsocketfactory;
    }

    /**
     * 创建信任所有Hostname的Verifier
     *
     * @return TrustAllHostnameVerifier
     */
    public static TrustAllHostnameVerifier createTrustAllHostnameVerifier() {
        return new TrustAllHostnameVerifier();
    }
    // ***私有方法***

    /**
     * build表单参数
     *
     * @param reqParams reqParams
     * @return FormBody extends RequestBody
     */
    private static FormBody buildFormBodyParams(JSONObject reqParams) {
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (reqParams != null && reqParams.size() > 0) {
            for (Map.Entry<String, Object> entry : reqParams.entrySet()) {
                formBodyBuilder.addEncoded(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }
        return formBodyBuilder.build();
    }

    /**
     * build挂在URL后面的参数
     *
     * @param urlParams urlParams
     * @return 如"?xxx=xxx&xxx=xxx"，且Encode
     */
    private static String buildUrlParams(JSONObject urlParams) {
        if (urlParams != null && urlParams.size() > 0) {
            StringBuilder urlParamsStringBuilder = new StringBuilder("?");
            Set<Map.Entry<String, Object>> entrySet = urlParams.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                try {
                    urlParamsStringBuilder.append(urlParamsStringBuilder.length() > 1 ? "&" : "").append(entry.getKey()).append("=").append(URLEncoder.encode(String.valueOf(entry.getValue()), StandardCharsets.UTF_8.name()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return "";
                }
            }
            String urlParamsString = urlParamsStringBuilder.toString();
            return urlParamsString.length() > 1 ? urlParamsString : "";
        }
        return "";
    }

    /**
     * 核心方法
     * 构建同步Request并返回Response
     *
     * @param urlStr       urlStr
     * @param method       method
     * @param requestBody  requestBody(GET时被忽略)
     * @param headerMap    headerMap(原有的)
     * @param addHeaderMap addHeaderMap(新增的)
     * @return Response
     */
    private static Response buildSyncRequestAndReturnResponse(String urlStr, byte method, RequestBody requestBody, Map<String, String> headerMap, Map<String, String> addHeaderMap) {
        return buildSyncRequestAndReturnResponse(urlStr, method, requestBody, headerMap, addHeaderMap, null, null);
    }

    /**
     * 核心方法
     * 构建同步Request并返回Response
     *
     * @param urlStr           urlStr
     * @param method           method
     * @param requestBody      requestBody(GET时被忽略)
     * @param headerMap        headerMap(原有的)
     * @param addHeaderMap     addHeaderMap(新增的)
     * @param sslSocketFactory sslSocketFactory
     * @param hostnameVerifier hostnameVerifier
     * @return Response
     */
    private static Response buildSyncRequestAndReturnResponse(String urlStr, byte method, RequestBody requestBody, Map<String, String> headerMap, Map<String, String> addHeaderMap, SSLSocketFactory sslSocketFactory, HostnameVerifier hostnameVerifier) {
        Request.Builder builder = new Request.Builder().url(urlStr);
        if (GET != method) {
            builder.post(requestBody);
        }
        // 修改原先的header
        if (headerMap != null && headerMap.size() > 0) {
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                builder = builder.header(entry.getKey(), entry.getValue());
            }
        }
        // 添加新的header
        if (addHeaderMap != null && addHeaderMap.size() > 0) {
            for (Map.Entry<String, String> entry : addHeaderMap.entrySet()) {
                builder = builder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        try {
            if (sslSocketFactory != null && hostnameVerifier != null) {
                OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                return clientBuilder.sslSocketFactory(sslSocketFactory, new TrustAllManager()).hostnameVerifier(hostnameVerifier).retryOnConnectionFailure(true).build().newCall(builder.build()).execute();
            } else {
                return new OkHttpClient().newCall(builder.build()).execute();
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 根据Set-Cookie的值截取JSESSIONID的值
     *
     * @param setCookie Set-Cookie的值
     * @return JSESSIONID(32为大写字符串)
     */
    private static String getJSESSIONIDBySetCookie(String setCookie) {
        if (StringUtils.isNotBlank(setCookie)) {
            Matcher matcher = J_SESSION_ID_REGEX.matcher(setCookie);
            if (matcher.find()) {
                return matcher.group().replace("JSESSIONID=", "");
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * 上传文件Bean
     */
    public static final class UploadFileBean {
        /**
         * 参数名
         */
        private String paramName;
        /**
         * 文件名
         */
        private String fileName;
        /**
         * 文件内容
         */
        private byte[] fileContentBytes;

        public UploadFileBean(String paramName, String fileName, byte[] fileContentBytes) {
            this.paramName = paramName;
            this.fileName = fileName;
            this.fileContentBytes = fileContentBytes;
        }

        public String getParamName() {
            return paramName;
        }

        public String getFileName() {
            return fileName;
        }

        public byte[] getFileContentBytes() {
            return fileContentBytes;
        }
    }

    // ****证书类相关****

    /**
     * 信任所有Manager类
     */
    public static class TrustAllManager implements X509TrustManager {

        /**
         * 空实现
         *
         * @param chain    chain
         * @param authType authType
         * @throws CertificateException CertificateException
         */
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        /**
         * 空实现
         *
         * @param chain    chain
         * @param authType authType
         * @throws CertificateException CertificateException
         */
        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        /**
         * 返回空对象
         *
         * @return new X509Certificate[0]
         */
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    /**
     * 信任所有主机名Verifier的类
     */
    public static class TrustAllHostnameVerifier implements HostnameVerifier {
        /**
         * 所有都信任
         *
         * @param hostname hostname
         * @param session  session
         * @return 总是true
         */
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
