package com.gitee.bomeng.commons.basictools.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * User:gfb
 * Date:2017/11/16
 * Desc:请求工具类
 *
 * @author gfb
 */
public class HttpResUtil {
    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(HttpResUtil.class);
    /**
     * MP3 FILE
     */
    public static int MP3_FILE = 0;
    /**
     * FILE_TYPE
     */
    private static final List[] FILE_TYPE = {
            // MP3_FILE
            new ArrayList<String>() {
                private static final long serialVersionUID = 6012373840220708048L;

                {
                    add("[audio/mpeg]");
                }
            }};
    private static boolean proxySet = false;
    private static String proxyHost = "127.0.0.1";
    private static int proxyPort = 8087;

    /**
     * GET
     */
    public static final int GET = 0;
    /**
     * POST
     */
    public static final int POST = 1;


    public static JSONObject sendSSLRequest(String url, JSONObject params, Integer methodType, Integer timeoutMillisecond) {
        return sendSSLRequest(url, params, methodType, timeoutMillisecond, "SSL", "SunJSSE");
    }

    public static JSONObject sendSSLRequest(String url, JSONObject params, Integer methodType, Integer timeoutMillisecond, String var0, String var1) {
        // check
        StringBuilder paramsStringBuilder = new StringBuilder();
        if (params != null) {
            for (Map.Entry<String, Object> entrySet : params.entrySet()) {
                // String.format的参数不能为null
                paramsStringBuilder.append(String.format("%s=%s", entrySet.getKey() == null ? "" : entrySet.getKey(), entrySet.getValue() == null ? "" : entrySet.getValue()));
            }
        }
        HttpsURLConnection httpsURLConnection = null;
        JSONObject rjo = null;
        try {
            httpsURLConnection = (HttpsURLConnection) new URL(url).openConnection();
            httpsURLConnection.setConnectTimeout(timeoutMillisecond);
            httpsURLConnection.setReadTimeout(timeoutMillisecond);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setUseCaches(false);
            if (Objects.equals(methodType, POST)) {
                httpsURLConnection.setRequestMethod("POST");
            }
            httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
            httpsURLConnection.setSSLSocketFactory(createSSLSocketFactory(var0, var1));
            if (Objects.equals(methodType, GET)) {
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.connect();
            }
            if (StringUtils.isNotBlank(paramsStringBuilder.toString())) {
                OutputStream retStr = httpsURLConnection.getOutputStream();
                retStr.write(paramsStringBuilder.toString().getBytes("UTF-8"));
                retStr.close();
            }
            rjo = JSON.parseObject(IOUtils.toString(httpsURLConnection.getInputStream(), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpsURLConnection != null) {
                httpsURLConnection.disconnect();
            }
        }
        return rjo;
    }

    private static SSLSocketFactory createSSLSocketFactory(String var0, String var1) throws NoSuchProviderException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = (var1 != null) ? SSLContext.getInstance(var0, var1) : SSLContext.getInstance(var0);
        X509TrustManager trustManager = new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
        sslContext.init((KeyManager[]) null, new TrustManager[]{trustManager}, new SecureRandom());
        return sslContext.getSocketFactory();
    }

    /**
     * 发送请求,返回的结果类型为JSON,如果不能转为JSON则为null
     *
     * @param url        URL
     * @param methodType 请求类型:POST或GET
     * @return JSON对象
     */
    public static JSONObject sendHttpRequest(String url, int methodType) {
        return sendHttpRequest(url, null, methodType, null);
    }

    /**
     * 发送请求
     *
     * @param url        URL
     * @param params     参数
     * @param methodType 请求类型:POST或GET
     * @return JSON对象
     */
    public static JSONObject sendHttpRequest(String url, JSONObject params, int methodType) {
        return sendHttpRequest(url, params, methodType, null);
    }

    /**
     * 发送请求
     *
     * @param url                URL
     * @param params             参数
     * @param methodType         请求类型:POST或GET
     * @param timeoutMillisecond 超时时间,毫秒
     * @return JSON对象
     */
    public static JSONObject sendHttpRequest(String url, JSONObject params, Integer methodType, Integer timeoutMillisecond) {
        // check
        if (StringUtils.isBlank(url)) {
            logger.error("sendHttpRequest参数中url=null");
            return null;
        }
        if (methodType == null) {
            logger.error("sendHttpRequest参数中httpType=null");
            return null;
        }
        if (timeoutMillisecond == null) {
            timeoutMillisecond = -1;
        }
        // do
        StringBuilder paramsStringBuilder = new StringBuilder();
        if (params != null) {
            for (Map.Entry<String, Object> entrySet : params.entrySet()) {
                // String.format的参数不能为null
                paramsStringBuilder.append(String.format("%s=%s", entrySet.getKey() == null ? "" : entrySet.getKey(), entrySet.getValue() == null ? "" : entrySet.getValue()));
            }
        }
        //  判断请求方式及请求前的准备工作
        HttpUriRequest httpUriRequest;
        if (Objects.equals(methodType, POST)) {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(
                    RequestConfig.custom().setSocketTimeout(timeoutMillisecond)
                            .setConnectTimeout(timeoutMillisecond).setConnectionRequestTimeout(timeoutMillisecond)
                            .build());
            StringEntity entity = new StringEntity(paramsStringBuilder.toString(), ContentType.create("application/x-www-form-urlencoded", "UTF-8"));
            httpPost.setEntity(entity);
            httpUriRequest = httpPost;
        } else if (Objects.equals(methodType, GET)) {
            HttpGet httpGet = new HttpGet(String.format("%s?%s", url, paramsStringBuilder.toString()));
            httpGet.setConfig(
                    RequestConfig.custom().setSocketTimeout(timeoutMillisecond)
                            .setConnectTimeout(timeoutMillisecond).setConnectionRequestTimeout(timeoutMillisecond)
                            .build());
            httpUriRequest = httpGet;
        } else {
            return null;
        }
        //  进行请求
        try {
            CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
            CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(httpUriRequest);
            if (closeableHttpResponse.getStatusLine().getStatusCode() != 200) {
                throw new Exception(String.format("请求失败[StatusLine:%s]", JSON.toJSONString(closeableHttpResponse.getStatusLine())));
            }
            return JSON.parseObject(EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据URL获取文件InputStream
     *
     * @param requestUrl URL
     * @return 输入流
     */
    public static InputStream getResIOByHttpGet(String requestUrl) {
        return getInputStreamByHttpGet(requestUrl, null);
    }

    /**
     * 根据URL获取文件InputStream
     *
     * @param requestUrl       URL
     * @param downloadFileType 文件类型
     * @return 输入流
     */
    public static InputStream getInputStreamByHttpGet(String requestUrl, Integer downloadFileType) {
        InputStream inputStream = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setRequestMethod("GET");
            httpUrlConn.connect();
            Map<String, List<String>> map = httpUrlConn.getHeaderFields();
            if (downloadFileType == null) {
                // 判断响应头是否是200
                if (httpUrlConn.getResponseCode() == 200) {
                    inputStream = httpUrlConn.getInputStream();
                }
            } else {
                if (httpUrlConn.getResponseCode() == 200 && FILE_TYPE[downloadFileType].contains(map.get("Content-Type").toString())) {
                    inputStream = httpUrlConn.getInputStream();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputStream;
    }

    public static String sendPost(String url, String param, boolean isproxy) {
        OutputStreamWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            HttpURLConnection conn = null;
            //使用代理模式
            if (isproxy) {
                @SuppressWarnings("static-access")
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
                conn = (HttpURLConnection) realUrl.openConnection(proxy);
            } else {
                conn = (HttpURLConnection) realUrl.openConnection();
            }
            // 打开和URL之间的连接
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // POST方法
            conn.setRequestMethod("POST");
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.connect();
            // 获取URLConnection对象对应的输出流
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            // 发送请求参数
            out.write(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }


}
