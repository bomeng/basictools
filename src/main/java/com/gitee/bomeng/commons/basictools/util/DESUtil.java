package com.gitee.bomeng.commons.basictools.util;

import com.gitee.bomeng.commons.basictools.util.ExceptionUtil;
import org.apache.commons.lang.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Objects;

/**
 * User:gfb
 * Date:2018/9/6
 * Desc:
 *
 * @author gfb
 */
public class DESUtil {

    /**
     * DES
     */
    private static final String DES = "DES";
    /**
     * 加密模式
     */
    private static final String MODEL_CBC = "CBC";
    /**
     * 填充方式
     */
    private static final String PADDING_PKCS5 = "PKCS5Padding";

    private DESUtil() {
    }


    /**
     * 加密方法
     *
     * @param content 要加密的文本
     * @param key     秘钥
     * @return 哈希字符串
     */
    public static String encryptToHexString(String content, String key) {
        return byteToHexString(Objects.requireNonNull(doDealWith(content.getBytes(), key.getBytes(), Cipher.ENCRYPT_MODE, MODEL_CBC, PADDING_PKCS5)));
    }


    /**
     * 加密方法
     *
     * @param content 要加密的文本
     * @param key     秘钥
     * @return base64字符串
     */
    public static String encryptToBase64String(String content, String key) {
        return Base64.getEncoder().encodeToString(doDealWith(content.getBytes(), key.getBytes(), Cipher.ENCRYPT_MODE, MODEL_CBC, PADDING_PKCS5));
    }

    /**
     * 解密方法
     *
     * @param content 哈希字符串
     * @param key     秘钥
     * @return 解密后文本
     */
    public static String decryptByHexString(String content, String key) {
        return new String(Objects.requireNonNull(doDealWith(hexStringToByte(content), key.getBytes(), Cipher.DECRYPT_MODE, MODEL_CBC, PADDING_PKCS5)));
    }

    /**
     * 解密方法
     *
     * @param content base64文本
     * @param key     秘钥
     * @return base64字符串
     */
    public static String decryptByBase64String(String content, String key) {
        return new String(Objects.requireNonNull(doDealWith(Base64.getDecoder().decode(content), key.getBytes(), Cipher.DECRYPT_MODE, MODEL_CBC, PADDING_PKCS5)));
    }

    /**
     * 加密或解密处理
     *
     * @param content          内容字节数组
     * @param keyBytes         秘钥字节数组
     * @param encryptOrDecrypt 加密或解密
     * @param model            加密或解密模式
     * @param padding          填充
     * @return 加密或解密后的字节数组
     */
    private static byte[] doDealWith(byte[] content, byte[] keyBytes, int encryptOrDecrypt, String model, String padding) {
        try {
            DESKeySpec desKeySpec = new DESKeySpec(keyBytes);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(DES);
            SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);
            Cipher cipher = Cipher.getInstance(String.format("%s/%s/%s", DES, model, padding));
            cipher.init(encryptOrDecrypt, secretKey, new IvParameterSpec(desKeySpec.getKey()));
            return cipher.doFinal(content);
        } catch (NoSuchAlgorithmException e) {
            ExceptionUtil.throwNewCommonRuntimeException("NoSuchAlgorithmException:" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            ExceptionUtil.throwNewCommonRuntimeException("InvalidKeyException:" + e.getMessage());
            return null;
        } catch (InvalidAlgorithmParameterException e) {
            ExceptionUtil.throwNewCommonRuntimeException("InvalidAlgorithmParameterException:" + e.getMessage());
            return null;
        } catch (NoSuchPaddingException e) {
            ExceptionUtil.throwNewCommonRuntimeException("NoSuchPaddingException:" + e.getMessage());
            return null;
        } catch (BadPaddingException e) {
            ExceptionUtil.throwNewCommonRuntimeException("BadPaddingException:" + e.getMessage());
            return null;
        } catch (InvalidKeySpecException e) {
            ExceptionUtil.throwNewCommonRuntimeException("InvalidKeySpecException:" + e.getMessage());
            return null;
        } catch (IllegalBlockSizeException e) {
            ExceptionUtil.throwNewCommonRuntimeException("IllegalBlockSizeException:" + e.getMessage());
            return null;
        }

    }

    /**
     * 将字节数组转为哈希字符串
     *
     * @param bytes byte数组
     * @return 哈希字符串
     */
    private static String byteToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length);
        for (byte aByte : bytes) {
            String sTemp = Integer.toHexString(255 & aByte);
            if (sTemp.length() < 2) {
                sb.append(0);
            }
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 字符转为字节
     *
     * @param c 字符
     * @return 字节
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    /**
     * 哈希字符串转为字节数组
     *
     * @param hexString 哈希字符串
     * @return 字节数组
     */
    private static byte[] hexStringToByte(String hexString) {
        if (StringUtils.isNotBlank(hexString)) {
            hexString = hexString.toUpperCase();
            int length = hexString.length() / 2;
            char[] hexChars = hexString.toCharArray();
            byte[] d = new byte[length];
            for (int i = 0; i < length; ++i) {
                int pos = i * 2;
                d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
            }
            return d;
        } else {
            return null;
        }
    }
}
