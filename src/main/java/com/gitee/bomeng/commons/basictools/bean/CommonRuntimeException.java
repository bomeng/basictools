package com.gitee.bomeng.commons.basictools.bean;

/**
 * User:gfb
 * Date:2018/6/13
 * Desc:
 */
public class CommonRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -6669767141032947671L;

    private Integer errorCode;
    private String errorMessage;

    public CommonRuntimeException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public CommonRuntimeException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
