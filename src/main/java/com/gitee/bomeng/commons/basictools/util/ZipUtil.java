package com.gitee.bomeng.commons.basictools.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.*;

/**
 * User:gfb
 * Date:2018/7/1
 * desc:对文件或文件夹进行压缩或解压
 *
 * @author GFB
 */
public class ZipUtil {

    /**
     * 对文件或文件目录进行压缩
     *
     * @param srcPath     要压缩的源文件路径。如果压缩一个文件，则为该文件的全路径；如果压缩一个目录，则为该目录的顶层目录路径
     * @param zipPath     压缩文件保存的路径。注意：zipPath不能是srcPath路径下的子文件夹
     * @param zipFileName 压缩文件名
     * @throws Exception 异常
     */
    public static void zip(String srcPath, String zipPath, String zipFileName) throws Exception {
        if (StringUtils.isEmpty(srcPath) || StringUtils.isEmpty(zipPath) || StringUtils.isEmpty(zipFileName)) {
            ExceptionUtil.throwNewCommonRuntimeException("srcPath或zipPath或zipFileName参数为空");
        }
        ZipOutputStream zos = null;
        try {
            File srcFile = new File(srcPath);
            //判断压缩文件保存的路径是否为源文件路径的子文件夹，如果是，则抛出异常（防止无限递归压缩的发生）
            if (srcFile.isDirectory() && zipPath.contains(srcPath)) {
                ExceptionUtil.throwNewCommonRuntimeException("zipPath不能是srcPath路径下的子文件夹");
            }
            //判断压缩文件保存的路径是否存在，如果不存在，则创建目录
            File zipDir = new File(zipPath);
            if (!zipDir.exists() || !zipDir.isDirectory()) {
                zipDir.mkdirs();
            }
            //创建压缩文件保存的文件对象
            String zipFilePath = zipPath + File.separator + zipFileName;
            File zipFile = new File(zipFilePath);
            if (zipFile.exists()) {
                //删除已存在的目标文件
                zipFile.delete();
            }
            CheckedOutputStream cos = new CheckedOutputStream(new FileOutputStream(zipFile), new CRC32());
            zos = new ZipOutputStream(cos);
            //如果只是压缩一个文件，则需要截取该文件的父目录
            String srcRootDir = srcPath;
            if (srcFile.isFile()) {
                int index = srcPath.lastIndexOf(File.separator);
                if (index != -1) {
                    srcRootDir = srcPath.substring(0, index);
                }
            }
            //调用递归压缩方法进行目录或文件压缩
            zip(srcRootDir, srcFile, zos);
            zos.flush();
        } finally {
            try {
                if (zos != null) {
                    zos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 解压缩zip包
     *
     * @param zipFilePath        zip文件的全路径
     * @param unzipFilePath      解压后的文件保存的路径
     * @param includeZipFileName 解压后的文件保存的路径是否包含压缩文件的文件名。true-包含；false-不包含
     * @throws Exception 异常
     */
    public static void unzip(String zipFilePath, String unzipFilePath, boolean includeZipFileName) throws Exception {
        if (StringUtils.isEmpty(zipFilePath) || StringUtils.isEmpty(unzipFilePath)) {
            ExceptionUtil.throwNewCommonRuntimeException("zipFilePath或unzipFilePath为空");
        }
        File zipFile = new File(zipFilePath);
        //如果解压后的文件保存路径包含压缩文件的文件名，则追加该文件名到解压路径
        if (includeZipFileName) {
            String fileName = zipFile.getName();
            if (StringUtils.isNotEmpty(fileName)) {
                fileName = fileName.substring(0, fileName.lastIndexOf("."));
            }
            unzipFilePath = unzipFilePath + File.separator + fileName;
        }
        //创建解压缩文件保存的路径
        File unzipFileDir = new File(unzipFilePath);
        if (!unzipFileDir.exists()) {
            unzipFileDir.mkdirs();
        }
        // 开始解压
        String entryFilePath;
        File entryFile;
        OutputStream outputStream;
        InputStream inputStream;
        ZipFile zip = new ZipFile(zipFile);
        Enumeration<? extends ZipEntry> entries = zip.entries();
        ZipEntry entry;
        //循环对压缩包里的每一个文件进行解压
        while (entries.hasMoreElements()) {
            entry = entries.nextElement();
            //构建压缩包中一个文件解压后保存的文件全路径
            entryFilePath = unzipFilePath + File.separator + entry.getName();
            entryFile = new File(entryFilePath);
            // 判断文件夹或者文件
            if (entryFilePath.endsWith(File.separator) || entryFilePath.endsWith("/")) {
                // 文件夹
                if (!entryFile.exists()) {
                    entryFile.mkdirs();
                }
            } else {
                // 文件
                if (!entryFile.exists()) {
                    createNewFile(entryFile);
                }
                // 写入文件
                inputStream = zip.getInputStream(entry);
                outputStream = new FileOutputStream(entryFile);
                IOUtils.copyLarge(inputStream, outputStream);
                inputStream.close();
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    /**
     * 递归压缩文件夹
     *
     * @param srcRootPath 压缩文件夹根目录的子路径
     * @param file        当前递归压缩的文件或目录对象
     * @param zos         压缩文件存储对象
     * @throws Exception 异常
     */
    private static void zip(String srcRootPath, File file, ZipOutputStream zos) throws Exception {
        if (file == null) {
            return;
        }
        //如果是文件，则直接压缩该文件
        if (file.isFile()) {
            int count, bufferLen = 1024;
            byte data[] = new byte[bufferLen];

            //获取文件相对于压缩文件夹根目录的子路径
            String subPath = file.getAbsolutePath();
            int index = subPath.indexOf(srcRootPath);
            if (index != -1) {
                subPath = subPath.substring(srcRootPath.length() + File.separator.length());
            }
            ZipEntry entry = new ZipEntry(subPath);
            zos.putNextEntry(entry);
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            while ((count = bis.read(data, 0, bufferLen)) != -1) {
                zos.write(data, 0, count);
            }
            bis.close();
            zos.closeEntry();
        }
        //如果是目录，则压缩整个目录
        else {
            //压缩目录中的文件或子目录
            File[] childFileList = file.listFiles();
            if (childFileList != null) {
                if (childFileList.length == 0) {
                    // 空文件情况
                    //获取文件相对于压缩文件夹根目录的子路径
                    String subPath = file.getAbsolutePath() + File.separator;
                    int index = subPath.indexOf(srcRootPath);
                    if (index != -1) {
                        subPath = subPath.substring(srcRootPath.length() + File.separator.length());
                    }
                    ZipEntry entry = new ZipEntry(subPath);

                    zos.putNextEntry(entry);
                    zos.closeEntry();
                } else {
                    for (File childFile : childFileList) {
                        zip(srcRootPath, childFile, zos);
                    }
                }
            }
        }
    }


    /**
     * 创建文件，如果父级目录不存在则创建
     *
     * @param file file
     * @throws IOException 异常
     */
    public static void createNewFile(File file) throws IOException {
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        file.createNewFile();
    }
}
