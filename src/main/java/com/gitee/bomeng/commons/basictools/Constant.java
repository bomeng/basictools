package com.gitee.bomeng.commons.basictools;

import java.nio.charset.StandardCharsets;

/**
 * User:gfb
 * Date:2018/5/22
 * Desc:
 */
public class Constant {

    /**
     * session中用户登录信息
     */
    public static final String SESSION_CURRENT_LOGIN_USER_INFO = "SESSION_CURRENT_LOGIN_USER_INFO";
    /**
     * 设置跨域
     */
    public static final String ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    /**
     * 设置JSON返回格式
     */
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    /**
     * 默认编码
     */
    public static final String DEFAULT_ENCODE_UTF8 = StandardCharsets.UTF_8.toString();


}
