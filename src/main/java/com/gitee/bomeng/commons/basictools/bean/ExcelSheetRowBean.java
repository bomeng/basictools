package com.gitee.bomeng.commons.basictools.bean;

import com.gitee.bomeng.commons.basictools.util.ExceptionUtil;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * User:gfb
 * Date:2018/6/14
 * Desc:工作表中的行数据,一行对应一个ExcelSheetRowBean对象,实现为HashMap&lt;Integer,String&gt;,列名(索引)=&gt;Integer(从0开始);列值=&gt;String
 *
 * @author gfb
 */
public class ExcelSheetRowBean extends HashMap<Integer, String> {
    private static final long serialVersionUID = -3527133160657139684L;

    /**
     * 常用Excel字母列
     */
    public static class CellIndex {
        public static int A = 0;
        public static int B = 1;
        public static int C = 2;
        public static int D = 3;
        public static int E = 4;
        public static int F = 5;
        public static int G = 6;
        public static int H = 7;
        public static int I = 8;
        public static int J = 9;
        public static int K = 10;
        public static int L = 11;
        public static int M = 12;
        public static int N = 13;
        public static int O = 14;
        public static int P = 15;
        public static int Q = 16;
        public static int R = 17;
        public static int S = 18;
        public static int T = 19;
        public static int U = 20;
        public static int V = 21;
        public static int W = 22;
        public static int X = 23;
        public static int Y = 24;
        public static int Z = 25;
    }

    /**
     * Excel列的字母正则表达式
     */
    private static Pattern pattern = Pattern.compile("[A-Z]+");
    /**
     * 26进制
     */
    private static int MAX_INDEX = 26;

    public ExcelSheetRowBean(int initialCapacity) {
        super(initialCapacity);
    }

    public ExcelSheetRowBean() {
    }

    /**
     * 根据指定单元格值生成对象
     *
     * @param args 单元格的值，不需要指定列明
     */
    public ExcelSheetRowBean(String... args) {
        this((int) (args.length / .75 + 1));
        for (int i = 0; i < args.length; i++) {
            put(i, String.valueOf(args[i]));
        }
    }

    /**
     * 增加某列值
     *
     * @param columnNum 列
     * @param value     值
     * @return 上一次被替换的值, 如果没有为null
     */
    @Override
    public String put(Integer columnNum, String value) {
        return super.put(columnNum, value);
    }

    /**
     * 增加某列值
     *
     * @param columnStr 列
     * @param value     值
     * @return 上一次被替换的值, 如果没有为null
     */
    public String put(String columnStr, String value) {
        return super.put(convertStringToColumnNum(columnStr), value);
    }

    /**
     * 根据列的索引获取值
     *
     * @param columnNum 列的索引值,从0开始
     * @return 某列的值
     */
    public String get(Integer columnNum) {
        return super.get(columnNum);
    }

    /**
     * 根据Excel的列的字母获取值
     *
     * @param columnStr 列的字母值,比如:A或B..
     * @return 某列的值
     */
    public String get(String columnStr) {
        return get(convertStringToColumnNum(columnStr));
    }

    /**
     * 将Excel的字母列转为数值索引
     *
     * @param columnStr 字母列
     * @return 数值索引
     */
    private int convertStringToColumnNum(String columnStr) {
        if (StringUtils.isBlank(columnStr) || !pattern.matcher((columnStr = columnStr.toUpperCase())).matches()) {
            ExceptionUtil.throwNewCommonRuntimeException("参数columnStr不合法");
        }
        int columnNum = 0;
        for (int i = columnStr.length() - 1; i >= 0; i--) {
            columnNum += i * MAX_INDEX + (columnStr.charAt(i) - 'A');
        }
        return columnNum;
    }
}
