package com.gitee.bomeng.commons.basictools.util;

import java.math.BigDecimal;

/**
 * User:gfb
 * Date:2018/6/13
 * Desc: 小数运算工具
 *
 * @author gfb
 */
public class DecimalOperationUtil {

    private DecimalOperationUtil() {
    }

    /**
     * 加法运算
     *
     * @param f1 被加数
     * @param f2 加数
     * @return 和
     */
    public static float add(float f1, float f2) {
        BigDecimal b1 = new BigDecimal(Float.toString(f1));
        BigDecimal b2 = new BigDecimal(Float.toString(f2));
        return b1.add(b2).floatValue();
    }

    /**
     * 减法运算
     *
     * @param f1 被减数
     * @param f2 减数
     * @return 差
     */
    public static float subtract(float f1, float f2) {
        BigDecimal b1 = new BigDecimal(Float.toString(f1));
        BigDecimal b2 = new BigDecimal(Float.toString(f2));
        return b1.subtract(b2).floatValue();
    }

    /**
     * 乘法运算
     *
     * @param f1 被乘数
     * @param f2 乘数
     * @return 积
     */
    public static float multiply(float f1, float f2) {
        BigDecimal b1 = new BigDecimal(Float.toString(f1));
        BigDecimal b2 = new BigDecimal(Float.toString(f2));
        return b1.multiply(b2).floatValue();
    }

    /**
     * 除法运算
     *
     * @param f1    被除数
     * @param f2    除数
     * @param scale 保留几位小数
     * @return 商(最后一位四舍五入)
     */
    public static float divide(float f1, float f2, int scale) {
        if (scale < 0) {
            ExceptionUtil.throwNewCommonRuntimeException("scale < 0");
        }
        BigDecimal b1 = new BigDecimal(Float.toString(f1));
        BigDecimal b2 = new BigDecimal(Float.toString(f2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    /**
     * 加法运算
     *
     * @param d1 被加数
     * @param d2 加数
     * @return 和
     */
    public static double add(double d1, double d2) {
        BigDecimal b1 = new BigDecimal(Double.toString(d1));
        BigDecimal b2 = new BigDecimal(Double.toString(d2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 减法运算
     *
     * @param d1 被减数
     * @param d2 减数
     * @return 差
     */
    public static double subtract(double d1, double d2) {
        BigDecimal b1 = new BigDecimal(Double.toString(d1));
        BigDecimal b2 = new BigDecimal(Double.toString(d2));
        return b1.subtract(b2).doubleValue();
    }

    /**
     * 乘法运算
     *
     * @param d1 被乘数
     * @param d2 乘数
     * @return 积
     */
    public static double multiply(double d1, double d2) {
        BigDecimal b1 = new BigDecimal(Double.toString(d1));
        BigDecimal b2 = new BigDecimal(Double.toString(d2));
        return b1.multiply(b2).doubleValue();
    }

    /**
     * 除法运算
     *
     * @param d1    被除数
     * @param d2    除数
     * @param scale 保留几位小数
     * @return 商(最后一位四舍五入)
     */
    public static double divide(double d1, double d2, int scale) {
        if (scale < 0) {
            ExceptionUtil.throwNewCommonRuntimeException("scale < 0");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(d1));
        BigDecimal b2 = new BigDecimal(Double.toString(d2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 截取指定小数点位数
     * truncate(321.123456789, -2)) = 300.0
     * truncate(321.123456789, -1)) = 320.0
     * truncate(321.123456789, 0))  = 321.0
     * truncate(321.123456789, 1))  = 321.1
     * truncate(321.123456789, 2))  = 321.12
     *
     * @param d        要截取的小数
     * @param scaleNum 小数位数，可为负
     * @return 截取后的小数
     */
    public static double truncate(double d, int scaleNum) {
        double t = Math.pow(10, scaleNum);
        scaleNum = scaleNum < 0 ? 0 : scaleNum;
        if (d >= 0) {
            return DecimalOperationUtil.divide(Math.floor(DecimalOperationUtil.multiply(d, t)), t, scaleNum);
        } else {
            return -DecimalOperationUtil.divide(Math.floor(-DecimalOperationUtil.multiply(d, t)), t, scaleNum);
        }
    }

    /**
     * 截取小数点位数
     * truncateToString(987654321.123456789, -1)) = 987654320
     * truncateToString(987654321.123456789, 0))  = 987654321
     * truncateToString(987654321.123456789, 1))  = 987654321.1
     * truncateToString(987654321.123456789, 2))  = 987654321.12
     *
     * @param d        要截取的小数
     * @param scaleNum 小数位数，可为负
     * @return 截取后的小数字符串
     */
    public static String truncateToString(double d, int scaleNum) {
        return String.format("%.0" + ((scaleNum < 0) ? 0 : scaleNum) + "f", truncate(d, scaleNum));
    }

}
