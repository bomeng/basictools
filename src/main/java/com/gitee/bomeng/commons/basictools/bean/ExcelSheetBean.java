package com.gitee.bomeng.commons.basictools.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User:gfb
 * Date:2018/5/28
 * Desc: 工作表的bean，继承ArrayList&lt;ExcelSheetRowBean&gt;
 *
 * @author gfb
 */
public class ExcelSheetBean extends ArrayList<ExcelSheetRowBean> implements Serializable {
    private static final long serialVersionUID = 2017179406582067734L;
    /**
     * 工作表的名字
     */
    private String name;
    /**
     * 用户保存ExcelBean的引用
     */
    private ExcelBean excelBean;

    public ExcelSheetBean() {
    }

    public ExcelSheetBean(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * 设置所属ExcelBean的引用,禁止外界调用
     *
     * @param excelBean ExcelBean
     */
    void setExcelBean(ExcelBean excelBean) {
        this.excelBean = excelBean;
    }

    /**
     * 获取工作表名称
     *
     * @return 工作表名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置工作表名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 设置sheetNum
     *
     * @param id sheetNum,工作表的第几个，从1开始
     * @return boolean
     */
    public boolean setSheetNum(int id) {
        if (id > excelBean.size()) {
            return false;
        }
        if (!excelBean.remove(this)) {
            return false;
        }
        excelBean.addExcelSheetBean(id, this);
        return true;
    }

    /**
     * 获取sheetNum，从1开始
     *
     * @return int
     */
    public int getSheetNum() {
        return excelBean.indexOf(this) + 1;
    }

    /**
     * 增加工作表的行数据
     *
     * @param excelSheetRowBean 工作表行数据
     * @return 工作表
     */
    public ExcelSheetBean addExcelSheetRowBeanAndReturnExcelSheetBean(ExcelSheetRowBean excelSheetRowBean) {
        super.add(excelSheetRowBean);
        return this;
    }
}
