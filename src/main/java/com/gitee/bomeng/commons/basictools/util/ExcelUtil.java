package com.gitee.bomeng.commons.basictools.util;

import com.gitee.bomeng.commons.basictools.Enum.ExcelTypeEnum;
import com.gitee.bomeng.commons.basictools.bean.ExcelBean;
import com.gitee.bomeng.commons.basictools.bean.ExcelSheetBean;
import com.gitee.bomeng.commons.basictools.bean.ExcelSheetRowBean;
import com.gitee.bomeng.commons.basictools.util.ExceptionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * Created by gfb on 2017/9/7.
 *
 * @author gfb
 */
public class ExcelUtil {

    /**
     * 读取Excel中的所有的数据,如果数据有规律,可以指定size进行优化
     *
     * @param inputStream   Excel文件输入流
     * @param excelTypeEnum Excel扩展名
     * @return 返回Excel对象
     * @throws Exception 异常
     */
    public static ExcelBean readFullyExcel(InputStream inputStream, ExcelTypeEnum excelTypeEnum) throws Exception {
        ExcelBean excelBean = new ExcelBean();
        // 保存扩展名类型
        excelBean.setExcelTypeEnum(excelTypeEnum);
        // 低版本的Excel文件读取
        if (ExcelTypeEnum.XLS.equals(excelTypeEnum)) {
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(inputStream);
            for (int i = 0; i < hssfWorkbook.getNumberOfSheets(); i++) {
                HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(i);
                if (hssfSheet == null) {
                    ExceptionUtil.throwNewCommonException("工作表不存在[i:%s]", i);
                }
                // getLastRowNum()方法返回的是最后一行的index(从0开始),不是size
                int rowSize = hssfSheet.getLastRowNum() + 1;
                ExcelSheetBean excelSheetBean = new ExcelSheetBean(rowSize);
                excelBean.add(excelSheetBean);
                excelSheetBean.setName(hssfSheet.getSheetName());
                for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {
                    HSSFRow hssfRow = hssfSheet.getRow(rowIndex);
                    if (hssfRow != null) {
                        int maxColumnNum = hssfRow.getLastCellNum();
                        ExcelSheetRowBean excelSheetRowBean = new ExcelSheetRowBean(new Float(maxColumnNum / 0.75f + 1).intValue());
                        for (int cellIndex = 0; cellIndex < maxColumnNum; cellIndex++) {
                            HSSFCell xssfCell = hssfRow.getCell(cellIndex);
                            excelSheetRowBean.put(cellIndex, (xssfCell == null ? "" : getValue(xssfCell)));
                        }
                        excelSheetBean.add(excelSheetRowBean);
                    }
                }
            }
            return excelBean;
        }
        // 高版本的Excel文件读取
        if (ExcelTypeEnum.XLSX.equals(excelTypeEnum)) {
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
            // 遍历每一个工作表
            for (int i = 0; i < xssfWorkbook.getNumberOfSheets(); i++) {
                XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(i);
                if (xssfSheet == null) {
                    ExceptionUtil.throwNewCommonException("工作表不存在[i:%s]", i);
                }
                int rowSize = xssfSheet.getLastRowNum() + 1;
                ExcelSheetBean excelSheetBean = new ExcelSheetBean();
                excelBean.add(excelSheetBean);
                excelSheetBean.setName(xssfSheet.getSheetName());
                // 遍历工作表的每一行
                for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {
                    XSSFRow xssfRow = xssfSheet.getRow(rowIndex);
                    if (xssfRow != null) {
                        int maxColumnNum = xssfRow.getLastCellNum();
                        // 遍历每一行的每一列
                        ExcelSheetRowBean excelSheetRowBean = new ExcelSheetRowBean(new Float(maxColumnNum / 0.75f + 1).intValue());
                        for (int cellIndex = 0; cellIndex < maxColumnNum; cellIndex++) {
                            XSSFCell xssfCell = xssfRow.getCell(cellIndex);
                            excelSheetRowBean.put(cellIndex, (xssfCell == null ? "" : getValue(xssfCell)));
                        }
                        excelSheetBean.add(excelSheetRowBean);
                    }
                }
            }
            return excelBean;
        }
        return null;
    }

    /**
     * 写出Excel到Response对象
     *
     * @param excelBean Excel对象
     * @param response  HttpServletResponse
     * @throws Exception 异常
     */
    public static void writeFullyExcelToResponse(ExcelBean excelBean, HttpServletResponse response) throws Exception {
        if (response == null) {
            ExceptionUtil.throwNewCommonException("Response对象为null");
        }
        response.setContentType("application/vnd.ms-excel");
        response.addHeader("Content-disposition", "attachment;filename=download.xlsx");
        writeFullyExcel(excelBean, response.getOutputStream());
    }

    /**
     * 写出Excel。注意:此流尚未进行关闭
     *
     * @param excelBean    Excel对象
     * @param outputStream 输出流
     * @throws Exception 抛出异常
     */
    public static void writeFullyExcel(ExcelBean excelBean, OutputStream outputStream) throws Exception {
        if (excelBean == null) {
            ExceptionUtil.throwNewCommonException("ExcelBean对象为null");
        }
        if (outputStream == null) {
            ExceptionUtil.throwNewCommonException("OutputStream对象为null");
        }
        // 如果Excel的扩展名名没有设置,则默认置为XLSX
        if (excelBean.getExcelTypeEnum() == null) {
            excelBean.setExcelTypeEnum(ExcelTypeEnum.XLSX);
        }
        // 写出XLSX类型的文件
        if (ExcelTypeEnum.XLSX.equals(excelBean.getExcelTypeEnum())) {
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
            // 写入每个工作表
            for (int i = 0; i < excelBean.size(); i++) {
                ExcelSheetBean excelSheetBean = excelBean.get(i);
                if (excelSheetBean == null) {
                    continue;
                }
                if (StringUtils.isBlank(excelSheetBean.getName())) {
                    excelSheetBean.setName(String.format("Sheet%s", i));
                }
                XSSFSheet xssfSheet = xssfWorkbook.createSheet(excelSheetBean.getName());
                // 写入每行
                for (int rowIndex = 0; rowIndex < excelSheetBean.size(); rowIndex++) {
                    XSSFRow xssfRow = xssfSheet.createRow(rowIndex);
                    // 写入每列
                    for (Map.Entry<Integer, String> entrySet : excelSheetBean.get(rowIndex).entrySet()) {
                        XSSFCell xssfCell = xssfRow.createCell(entrySet.getKey());
                        xssfCell.setCellType(CellType.STRING);
                        xssfCell.setCellValue(entrySet.getValue());
                    }
                }
            }
            xssfWorkbook.write(outputStream);
            outputStream.flush();
            return;
        }
        // 写出LS类型的文件
        if (ExcelTypeEnum.XLS.equals(excelBean.getExcelTypeEnum())) {
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            // 写入每个工作表
            for (int i = 0; i < excelBean.size(); i++) {
                ExcelSheetBean excelSheetBean = excelBean.get(i);
                if (StringUtils.isBlank(excelSheetBean.getName())) {
                    excelSheetBean.setName(String.format("Sheet%s", i));
                }
                HSSFSheet hssfSheet = hssfWorkbook.createSheet(excelSheetBean.getName());
                // 写入每行
                for (int rowIndex = 0; rowIndex < excelSheetBean.size(); rowIndex++) {
                    HSSFRow hssfRow = hssfSheet.createRow(rowIndex);
                    // 写入每列
                    for (Map.Entry<Integer, String> entrySet : excelSheetBean.get(rowIndex).entrySet()) {
                        HSSFCell hssfCell = hssfRow.createCell(entrySet.getKey());
                        hssfCell.setCellType(CellType.STRING);
                        hssfCell.setCellValue(entrySet.getValue());
                    }
                }
            }
            hssfWorkbook.write(outputStream);
            outputStream.flush();
        }
    }

    /**
     * 读取XSSFCell单元格数据为字符串类型的值
     *
     * @param xssfCell 单元格
     * @return 单元格字符串类型的值
     */
    private static String getValue(XSSFCell xssfCell) {
        xssfCell.setCellType(CellType.STRING);
        return xssfCell.getStringCellValue();
    }

    /**
     * 读取HSSFCell单元格数据为字符串类型的值
     *
     * @param hssfCell 单元格
     * @return 单元格字符串类型的值
     */
    private static String getValue(HSSFCell hssfCell) {
        hssfCell.setCellType(CellType.STRING);
        return hssfCell.getStringCellValue();
    }

}
