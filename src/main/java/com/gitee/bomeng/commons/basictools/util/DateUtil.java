package com.gitee.bomeng.commons.basictools.util;

import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * User:gfb
 * Date:2018/5/25
 * Desc:日期工具类
 *
 * @author gfb
 */
public class DateUtil {
    /**
     * 一天的毫秒数
     */
    private static final int ONE_DAY_MILLISECOND = 24 * 60 * 60 * 1000;

    /**
     * 日期格式数组
     */
    public static final String[] DATE_FORMAT_ARRAY = {"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "yyyyMMddHHmmss", "yyyy年MM月dd日 HH时mm分ss秒", "yyyy年MM月dd日"};

    private DateUtil() {
    }

    /**
     * 获取yyyy-MM-dd HH:mm:ss格式的时间字符串
     *
     * @param date date
     * @return 如2018-01-01 10:10:10
     */
    public static String getDateString(Date date) {
        return getDateString(date, DATE_FORMAT_ARRAY[0]);
    }

    /**
     * 获取Date的字符串形式
     *
     * @param date             date
     * @param dateStringFormat 如yyyy-MM-dd HH:mm:ss
     * @return 2018-01-01 10:10:10
     */
    public static String getDateString(Date date, String dateStringFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateStringFormat);
        return simpleDateFormat.format(date);
    }

    /**
     * 解析日期，支持yyyyMMddHHmmss和yyyy-MM-dd HH:mm:ss格式
     *
     * @param dateString 日期字符串
     * @return Date，如果解析失败返回null
     */
    public static Date parseDate(String dateString) {
        try {
            return DateUtils.parseDate(dateString, DATE_FORMAT_ARRAY);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 比较两个日期相差的天数,同一天返回0
     *
     * @param firstDate  第一个日期
     * @param secondDate 第二个日期
     * @return firstDate 大于 secondDate 时 返回正数
     */
    public static int getDifferDaysNumFrom2Date(Date firstDate, Date secondDate) {
        return (int) ((DateUtils.truncate(firstDate, Calendar.DATE).getTime() - DateUtils.truncate(secondDate, Calendar.DATE).getTime()) / ONE_DAY_MILLISECOND);
    }


    /**
     * 根据指定的日期获取当月第几日
     * 如12月19日则返回19
     *
     * @param date 日期
     * @return 本月第几日
     */
    public static int getDayNumInMonthByDate(Date date) {
        Calendar calendar = DateUtils.toCalendar(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取本月第几日
     * 如12月19日则返回19
     *
     * @return 本月第几日
     */
    public static int getDayNumInThisMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 根据指定日期获取当周星期几
     *
     * @param date 日期
     * @return 星期几
     */
    public static int getDayNumInWeekByDate(Date date) {
        Calendar calendar = DateUtils.toCalendar(date);
        int dayNumInThisWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayNumInThisWeek <= 0) {
            dayNumInThisWeek = 7;
        }
        return dayNumInThisWeek;
    }

    /**
     * 获取本周星期几
     *
     * @return 星期几
     */
    public static int getDayNumInThisWeek() {
        Calendar calendar = Calendar.getInstance();
        int dayNumInThisWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayNumInThisWeek <= 0) {
            dayNumInThisWeek = 7;
        }
        return dayNumInThisWeek;
    }
}
