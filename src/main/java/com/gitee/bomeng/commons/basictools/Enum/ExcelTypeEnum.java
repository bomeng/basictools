package com.gitee.bomeng.commons.basictools.Enum;

/**
 * User:gfb
 * Date:2018/5/28
 * Desc: Excel扩展名类型
 *
 * @author gfb
 */
public enum ExcelTypeEnum {
    /**
     * XLS的扩展名
     */
    XLS("xls"),
    /**
     * XLSX的扩展名
     */
    XLSX("xlsx");

    /**
     * 扩展名
     */
    private String extensionName;

    ExcelTypeEnum(String extensionName) {
        this.extensionName = extensionName;
    }

    public String getExtensionName() {
        return extensionName;
    }
}
